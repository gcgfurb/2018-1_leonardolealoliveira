﻿namespace TCC.Forms
{
    partial class ConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxFonteSize = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.buttonCorRetangulo = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxEspessura = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonCorFonte = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxFonte = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxVideosExt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxFacesExt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxFaces = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxVideos = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxGridY = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxGridX = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxNeighbors = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxRadius = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxThreshold = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxEscalaRedimensionamento = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxTamanhoRetanguloFace = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxHaarCascade = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxFonteSize);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.buttonCorRetangulo);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBoxEspessura);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.buttonCorFonte);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxFonte);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(3, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 148);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Interface";
            // 
            // textBoxFonteSize
            // 
            this.textBoxFonteSize.Location = new System.Drawing.Point(129, 41);
            this.textBoxFonteSize.Name = "textBoxFonteSize";
            this.textBoxFonteSize.Size = new System.Drawing.Size(265, 20);
            this.textBoxFonteSize.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 48);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Tamanho Fonte:";
            // 
            // buttonCorRetangulo
            // 
            this.buttonCorRetangulo.Location = new System.Drawing.Point(129, 118);
            this.buttonCorRetangulo.Name = "buttonCorRetangulo";
            this.buttonCorRetangulo.Size = new System.Drawing.Size(265, 20);
            this.buttonCorRetangulo.TabIndex = 9;
            this.buttonCorRetangulo.UseVisualStyleBackColor = true;
            this.buttonCorRetangulo.Click += new System.EventHandler(this.ButtonCorRetangulo_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Cor Retângulo:";
            // 
            // textBoxEspessura
            // 
            this.textBoxEspessura.Location = new System.Drawing.Point(129, 92);
            this.textBoxEspessura.Name = "textBoxEspessura";
            this.textBoxEspessura.Size = new System.Drawing.Size(265, 20);
            this.textBoxEspessura.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Espessura Retângulo:";
            // 
            // buttonCorFonte
            // 
            this.buttonCorFonte.Location = new System.Drawing.Point(129, 66);
            this.buttonCorFonte.Name = "buttonCorFonte";
            this.buttonCorFonte.Size = new System.Drawing.Size(265, 20);
            this.buttonCorFonte.TabIndex = 5;
            this.buttonCorFonte.UseVisualStyleBackColor = true;
            this.buttonCorFonte.Click += new System.EventHandler(this.ButtonCorFonte_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Cor Fonte:";
            // 
            // textBoxFonte
            // 
            this.textBoxFonte.Location = new System.Drawing.Point(129, 15);
            this.textBoxFonte.Name = "textBoxFonte";
            this.textBoxFonte.Size = new System.Drawing.Size(265, 20);
            this.textBoxFonte.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Nome Fonte:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxVideosExt);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBoxFacesExt);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.textBoxFaces);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.textBoxVideos);
            this.groupBox2.Location = new System.Drawing.Point(409, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(400, 122);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Arquivos";
            // 
            // textBoxVideosExt
            // 
            this.textBoxVideosExt.Location = new System.Drawing.Point(100, 66);
            this.textBoxVideosExt.Name = "textBoxVideosExt";
            this.textBoxVideosExt.Size = new System.Drawing.Size(294, 20);
            this.textBoxVideosExt.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Extensão Vídeos:";
            // 
            // textBoxFacesExt
            // 
            this.textBoxFacesExt.Location = new System.Drawing.Point(100, 92);
            this.textBoxFacesExt.Name = "textBoxFacesExt";
            this.textBoxFacesExt.Size = new System.Drawing.Size(294, 20);
            this.textBoxFacesExt.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Extensão Faces:";
            // 
            // textBoxFaces
            // 
            this.textBoxFaces.Location = new System.Drawing.Point(100, 40);
            this.textBoxFaces.Name = "textBoxFaces";
            this.textBoxFaces.Size = new System.Drawing.Size(294, 20);
            this.textBoxFaces.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Faces:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vídeos:";
            // 
            // textBoxVideos
            // 
            this.textBoxVideos.Location = new System.Drawing.Point(100, 15);
            this.textBoxVideos.Name = "textBoxVideos";
            this.textBoxVideos.Size = new System.Drawing.Size(294, 20);
            this.textBoxVideos.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxGridY);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.textBoxGridX);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.textBoxNeighbors);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.textBoxRadius);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.textBoxThreshold);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Location = new System.Drawing.Point(409, 129);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(400, 154);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Reconhecimento";
            // 
            // textBoxGridY
            // 
            this.textBoxGridY.Location = new System.Drawing.Point(100, 124);
            this.textBoxGridY.Name = "textBoxGridY";
            this.textBoxGridY.Size = new System.Drawing.Size(100, 20);
            this.textBoxGridY.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 131);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "GridY:";
            // 
            // textBoxGridX
            // 
            this.textBoxGridX.Location = new System.Drawing.Point(100, 98);
            this.textBoxGridX.Name = "textBoxGridX";
            this.textBoxGridX.Size = new System.Drawing.Size(100, 20);
            this.textBoxGridX.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 105);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "GridX:";
            // 
            // textBoxNeighbors
            // 
            this.textBoxNeighbors.Location = new System.Drawing.Point(100, 72);
            this.textBoxNeighbors.Name = "textBoxNeighbors";
            this.textBoxNeighbors.Size = new System.Drawing.Size(100, 20);
            this.textBoxNeighbors.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 79);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Neighbors:";
            // 
            // textBoxRadius
            // 
            this.textBoxRadius.Location = new System.Drawing.Point(100, 45);
            this.textBoxRadius.Name = "textBoxRadius";
            this.textBoxRadius.Size = new System.Drawing.Size(100, 20);
            this.textBoxRadius.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Radius:";
            // 
            // textBoxThreshold
            // 
            this.textBoxThreshold.Location = new System.Drawing.Point(100, 19);
            this.textBoxThreshold.Name = "textBoxThreshold";
            this.textBoxThreshold.Size = new System.Drawing.Size(100, 20);
            this.textBoxThreshold.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Threshold:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxEscalaRedimensionamento);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.textBoxTamanhoRetanguloFace);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.comboBoxHaarCascade);
            this.groupBox4.Location = new System.Drawing.Point(3, 155);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(400, 100);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Detecção";
            // 
            // textBoxEscalaRedimensionamento
            // 
            this.textBoxEscalaRedimensionamento.Location = new System.Drawing.Point(146, 72);
            this.textBoxEscalaRedimensionamento.Name = "textBoxEscalaRedimensionamento";
            this.textBoxEscalaRedimensionamento.Size = new System.Drawing.Size(248, 20);
            this.textBoxEscalaRedimensionamento.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(141, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Escala Redimensionamento:";
            // 
            // textBoxTamanhoRetanguloFace
            // 
            this.textBoxTamanhoRetanguloFace.Location = new System.Drawing.Point(146, 46);
            this.textBoxTamanhoRetanguloFace.Name = "textBoxTamanhoRetanguloFace";
            this.textBoxTamanhoRetanguloFace.Size = new System.Drawing.Size(248, 20);
            this.textBoxTamanhoRetanguloFace.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Tamanho Retângulo Face:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Haar Cascade:";
            // 
            // comboBoxHaarCascade
            // 
            this.comboBoxHaarCascade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaarCascade.FormattingEnabled = true;
            this.comboBoxHaarCascade.Location = new System.Drawing.Point(146, 19);
            this.comboBoxHaarCascade.Name = "comboBoxHaarCascade";
            this.comboBoxHaarCascade.Size = new System.Drawing.Size(248, 21);
            this.comboBoxHaarCascade.TabIndex = 1;
            // 
            // ConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 286);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ConfigForm";
            this.Text = "Configurações";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxVideos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxFaces;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFacesExt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxVideosExt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxFonte;
        private System.Windows.Forms.Button buttonCorFonte;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxEspessura;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonCorRetangulo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxHaarCascade;
        private System.Windows.Forms.TextBox textBoxTamanhoRetanguloFace;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxEscalaRedimensionamento;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxThreshold;
        private System.Windows.Forms.TextBox textBoxRadius;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxNeighbors;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxGridX;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxGridY;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxFonteSize;
        private System.Windows.Forms.Label label17;
    }
}