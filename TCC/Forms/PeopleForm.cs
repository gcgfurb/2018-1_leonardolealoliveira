﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Face;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using TCC.Data;
using TCC.Models;
using TCC.Util;

namespace TCC.Forms
{
    public partial class PeopleForm : Form
    {
        private System.Windows.Forms.Timer LoadTimer = new System.Windows.Forms.Timer();
        private Person SelectedPerson = null;
        private ContextMenuStrip contextMenuStrip = new ContextMenuStrip();
        private List<Person> people = new List<Person>();
        private string FilterFileName = "";

        public PeopleForm()
        {
            InitializeComponent();

            LoadTimer.Tick += LoadForm;
            ButtonClear_Click(null, null);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            DataFile.SaveFiles();
        }

        private void LoadForm(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            flowLayoutPanelPeople.Controls.Clear();

            for (int idx = 0; idx < people.Count; ++idx)
            {
                ImageBox imageBox = new ImageBox
                {
                    Image = people[idx].MainPhoto.Resize(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize, Emgu.CV.CvEnum.Inter.Cubic),
                    Size = new Size(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize),
                    FunctionalMode = ImageBox.FunctionalModeOption.Minimum,
                    Cursor = Cursors.Hand,
                    AllowDrop = true,
                };

                imageBox.MouseDown += Person_MouseDown;
                imageBox.DragEnter += Person_DragEnter;
                imageBox.DragDrop += Person_DragDrop;
                imageBox.MouseDoubleClick += Person_MouseDoubleClick;

                flowLayoutPanelPeople.Controls.Add(imageBox);
            }

            textBoxName.Enabled = people.Count > 0;
            if (textBoxName.Enabled)
                Person_MouseDoubleClick(flowLayoutPanelPeople.Controls[0], null);
            else
            {
                SelectedPerson = null;
                imageBoxPerson.Image = null;
                textBoxName.DataBindings.Clear();
                textBoxName.Text = "";
                flowLayoutPanelClones.Controls.Clear();
                flowLayoutPanelVideos.Controls.Clear();
            }

            contextMenuStrip.Items.Clear();
            contextMenuStrip.Items.Add("Remove");
            contextMenuStrip.ItemClicked += new ToolStripItemClickedEventHandler(ContextMenuStrip_ItemClicked);

            imageBoxPerson.FunctionalMode = ImageBox.FunctionalModeOption.Minimum;
            imageBoxFilter.FunctionalMode = ImageBox.FunctionalModeOption.Minimum;

            imageBoxFilter.AllowDrop = true;
            imageBoxFilter.DragEnter += ImageBoxFilter_DragEnter;
            imageBoxFilter.DragDrop += ImageBoxFilter_DragDrop;

            Cursor.Current = Cursors.Default;
            LoadTimer.Stop();
        }

        private void ImageBoxFilter_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;

            Array data = e.Data.GetData("FileDrop") as Array;
            if (data == null)
                return;

            if (data.Length != 1 || !(data.GetValue(0) is String))
                return;

            FilterFileName = ((string[])data)[0];
            FilterFileName = FilterFileName.ToLower();

            if (Path.GetExtension(FilterFileName) != ".png")
                FilterFileName = "";

            if (FilterFileName == "")
                return;

            e.Effect = DragDropEffects.Copy;
        }

        private void ImageBoxFilter_DragDrop(object sender, DragEventArgs e)
        {
            if (FilterFileName == "")
                return;

            Invoke((MethodInvoker)(() =>
            {
                imageBoxFilter.Image = new Image<Bgr, Byte>(FilterFileName).Resize(DataFile.config.FaceRectangleSize, DataFile.config.FaceRectangleSize, Emgu.CV.CvEnum.Inter.Cubic);
            }));
        }

        private void Person_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left || e.Clicks != 1)
                return;

            ImageBox source = (ImageBox)sender;
            int index = flowLayoutPanelPeople.Controls.IndexOf(source);

            DoDragDrop(source, DragDropEffects.Copy);
        }

        private void Person_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ImageBox)) && e.Data.GetData(typeof(ImageBox)) != (ImageBox)sender)
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void Person_DragDrop(object sender, DragEventArgs e)
        {
            ImageBox imageOrigin = (ImageBox)e.Data.GetData(typeof(ImageBox));
            ImageBox imageDestiny = (ImageBox)sender;

            int indexOrigin = flowLayoutPanelPeople.Controls.IndexOf(imageOrigin);
            int indexDestiny = flowLayoutPanelPeople.Controls.IndexOf(imageDestiny);

            Person personOrigin = people[indexOrigin];
            Person personDestiny = people[indexDestiny];

            flowLayoutPanelPeople.Controls.RemoveAt(indexOrigin);

            if (personDestiny == SelectedPerson)
            {
                ImageBox imageBox = new ImageBox
                {
                    Image = personOrigin.MainPhoto.Resize(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize, Emgu.CV.CvEnum.Inter.Cubic),
                    Size = new Size(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize),
                    FunctionalMode = ImageBox.FunctionalModeOption.Minimum,
                    ContextMenuStrip = contextMenuStrip,
                };

                flowLayoutPanelClones.Controls.Add(imageBox);

                foreach (Person clone in personOrigin.Clones)
                {
                    ImageBox imageBoxClone = new ImageBox
                    {
                        Image = clone.MainPhoto.Resize(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize, Emgu.CV.CvEnum.Inter.Cubic),
                        Size = new Size(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize),
                        FunctionalMode = ImageBox.FunctionalModeOption.Minimum,
                        ContextMenuStrip = contextMenuStrip,
                    };

                    flowLayoutPanelClones.Controls.Add(imageBoxClone);
                }
            }

            personDestiny.Clones.Add(personOrigin);
            foreach (Person clone in personOrigin.Clones)
                personDestiny.Clones.Add(clone);

            personOrigin.Clones.Clear();
            people.RemoveAt(indexOrigin);
        }

        private void Person_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ImageBox source = (ImageBox)sender;
            int index = flowLayoutPanelPeople.Controls.IndexOf(source);

            SelectedPerson = people[index];

            imageBoxPerson.Image = SelectedPerson.MainPhoto.Resize(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize, Emgu.CV.CvEnum.Inter.Cubic);

            textBoxName.DataBindings.Clear();
            textBoxName.DataBindings.Add("Text", SelectedPerson, "Name", false, DataSourceUpdateMode.OnPropertyChanged);

            flowLayoutPanelClones.Controls.Clear();
            flowLayoutPanelVideos.Controls.Clear();

            for (int idx = 0; idx < SelectedPerson.Clones.Count; ++idx)
            {
                ImageBox imageBox = new ImageBox
                {
                    Image = SelectedPerson.Clones[idx].MainPhoto.Resize(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize, Emgu.CV.CvEnum.Inter.Cubic),
                    Size = new Size(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize),
                    FunctionalMode = ImageBox.FunctionalModeOption.Minimum,
                    ContextMenuStrip = contextMenuStrip,
                };

                flowLayoutPanelClones.Controls.Add(imageBox);
            }

            foreach (KeyValuePair<string, Video> keyValue in DataFile.videos)
            {
                Video video = keyValue.Value;

                bool bFound = false;

                for (int idxFrame = 0; idxFrame < video.Frames.Count && !bFound; ++idxFrame)
                {
                    Frame frame = video.Frames[idxFrame];

                    for (int idxFace = 0; idxFace < frame.Faces.Count && !bFound; ++idxFace)
                    {
                        Face face = frame.Faces[idxFace];

                        if (face.IdPerson != SelectedPerson.Id)
                            continue;

                        ImageBox imageBox = new ImageBox
                        {
                            Image = video.Thumb,
                            Size = new Size(DataFile.config.VideoThumbSize, DataFile.config.VideoThumbSize),
                            FunctionalMode = ImageBox.FunctionalModeOption.Minimum,
                            Name = video.FilePath,
                        };

                        video.Showing = true;

                        imageBox.MouseDoubleClick += Video_MouseDoubleClick;
                        flowLayoutPanelVideos.Controls.Add(imageBox);

                        bFound = true;
                    }
                }
            }
        }

        private void Video_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ImageBox image = (ImageBox)sender;

            string hash = MD5Facade.CalculateMD5(image.Name);
            DataFile.videos.TryGetValue(hash, out Video video);
            video.Showing = true;

            Thread thread = new Thread(new ParameterizedThreadStart(RunVideoForm));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start(new KeyValuePair<string, int>(image.Name, SelectedPerson.Id));
        }

        private void RunVideoForm(object videoFilter)
        {
            KeyValuePair<string, int> keyValue = (KeyValuePair<string, int>)videoFilter;

            Application.Run(new VideoForm(keyValue.Key, keyValue.Value));
        }

        private void ContextMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip contextMenuStrip = (ContextMenuStrip)sender;
            ImageBox imagebox = (ImageBox)contextMenuStrip.SourceControl;
            int index = flowLayoutPanelClones.Controls.IndexOf(imagebox);
            if (index < 0)
                return;

            Person clone = SelectedPerson.Clones[index];

            SelectedPerson.Clones.RemoveAt(index);
            flowLayoutPanelClones.Controls.RemoveAt(index);
            people.Add(clone);

            ImageBox imageBox = new ImageBox
            {
                Image = clone.MainPhoto.Resize(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize, Emgu.CV.CvEnum.Inter.Cubic),
                Size = new Size(DataFile.config.PeopleFaceSize, DataFile.config.PeopleFaceSize),
                FunctionalMode = ImageBox.FunctionalModeOption.Minimum,
                Cursor = Cursors.Hand,
                AllowDrop = true,
            };

            imageBox.MouseDown += Person_MouseDown;
            imageBox.DragEnter += Person_DragEnter;
            imageBox.DragDrop += Person_DragDrop;
            imageBox.MouseDoubleClick += Person_MouseDoubleClick;

            flowLayoutPanelPeople.Controls.Add(imageBox);
        }

        private void ButtonApply_Click(object sender, EventArgs e)
        {
            people.Clear();
            people.AddRange(DataFile.people);

            FaceRecognizer FaceRecognizer = new LBPHFaceRecognizer(DataFile.config.LBPH_radius, DataFile.config.LBPH_neighbors, DataFile.config.LBPH_gridX, DataFile.config.LBPH_gridY, DataFile.config.LBPH_threshold);
            List<int> TrainLabels = new List<int>();
            List<Image<Gray, Byte>> TrainImages = new List<Image<Gray, byte>>();

            if (imageBoxFilter.Image != null)
            {
                DataFile.LoadTrainingData(TrainLabels, TrainImages);
                if (TrainImages.Count > 0)
                    FaceRecognizer.Train(TrainImages.ToArray(), TrainLabels.ToArray());

                Image<Bgr, Byte> image = new Image<Bgr, Byte>(imageBoxFilter.Image.Bitmap);

                FaceRecognizer.PredictionResult predictionResult = FaceRecognizer.Predict(image.Convert<Gray, Byte>());
                if (predictionResult.Label == 0)
                    people.Clear();
                else
                {
                    for (int idx = people.Count - 1; idx >= 0; --idx)
                    {
                        Person person = people[idx];

                        if (person.Id != predictionResult.Label)
                            people.RemoveAt(idx);
                    }
                }
            }

            for (int idx = people.Count - 1; idx >= 0; --idx)
            {
                Person person = people[idx];

                if (textBoxFilter.Text.Length > 0)
                {
                    if (!person.Name.ToLower().Contains(textBoxFilter.Text.ToLower()))
                    {
                        people.RemoveAt(idx);
                        continue;
                    }
                }
            }

            LoadTimer.Start();
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            people.Clear();
            people.AddRange(DataFile.people);

            textBoxFilter.Text = "";
            imageBoxFilter.Image = null;

            LoadTimer.Start();
        }
    }
}
