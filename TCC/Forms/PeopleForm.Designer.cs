﻿namespace TCC.Forms
{
    partial class PeopleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanelPeople = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanelVideos = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.imageBoxPerson = new Emgu.CV.UI.ImageBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanelClones = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonApply = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxFilter = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.imageBoxFilter = new Emgu.CV.UI.ImageBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBoxPerson)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBoxFilter)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanelPeople, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanelVideos, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.imageBoxPerson, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBoxName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanelClones, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 207F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(895, 532);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanelPeople
            // 
            this.flowLayoutPanelPeople.AutoScroll = true;
            this.flowLayoutPanelPeople.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanelPeople.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelPeople.Location = new System.Drawing.Point(3, 210);
            this.flowLayoutPanelPeople.Name = "flowLayoutPanelPeople";
            this.tableLayoutPanel1.SetRowSpan(this.flowLayoutPanelPeople, 5);
            this.flowLayoutPanelPeople.Size = new System.Drawing.Size(531, 319);
            this.flowLayoutPanelPeople.TabIndex = 1;
            // 
            // flowLayoutPanelVideos
            // 
            this.flowLayoutPanelVideos.AutoScroll = true;
            this.flowLayoutPanelVideos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanelVideos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelVideos.Location = new System.Drawing.Point(540, 401);
            this.flowLayoutPanelVideos.Name = "flowLayoutPanelVideos";
            this.flowLayoutPanelVideos.Size = new System.Drawing.Size(352, 128);
            this.flowLayoutPanelVideos.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(540, 382);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Vídeos:";
            // 
            // imageBoxPerson
            // 
            this.imageBoxPerson.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBoxPerson.Location = new System.Drawing.Point(540, 3);
            this.imageBoxPerson.Name = "imageBoxPerson";
            this.imageBoxPerson.Size = new System.Drawing.Size(200, 200);
            this.imageBoxPerson.TabIndex = 2;
            this.imageBoxPerson.TabStop = false;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(540, 210);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(197, 20);
            this.textBoxName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(540, 232);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Clones:";
            // 
            // flowLayoutPanelClones
            // 
            this.flowLayoutPanelClones.AutoScroll = true;
            this.flowLayoutPanelClones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanelClones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelClones.Location = new System.Drawing.Point(540, 251);
            this.flowLayoutPanelClones.Name = "flowLayoutPanelClones";
            this.flowLayoutPanelClones.Size = new System.Drawing.Size(352, 128);
            this.flowLayoutPanelClones.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.buttonApply);
            this.panel1.Controls.Add(this.buttonClear);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBoxFilter);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.imageBoxFilter);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(531, 201);
            this.panel1.TabIndex = 0;
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(448, 144);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(75, 23);
            this.buttonApply.TabIndex = 3;
            this.buttonApply.Text = "Aplicar";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.ButtonApply_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(448, 173);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 4;
            this.buttonClear.Text = "Limpar";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(205, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Face:";
            // 
            // textBoxFilter
            // 
            this.textBoxFilter.Location = new System.Drawing.Point(44, 5);
            this.textBoxFilter.Name = "textBoxFilter";
            this.textBoxFilter.Size = new System.Drawing.Size(150, 20);
            this.textBoxFilter.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nome:";
            // 
            // imageBoxFilter
            // 
            this.imageBoxFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBoxFilter.Location = new System.Drawing.Point(242, -1);
            this.imageBoxFilter.Name = "imageBoxFilter";
            this.imageBoxFilter.Size = new System.Drawing.Size(200, 200);
            this.imageBoxFilter.TabIndex = 2;
            this.imageBoxFilter.TabStop = false;
            // 
            // PeopleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 532);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "PeopleForm";
            this.Text = "Pessoas";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBoxPerson)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBoxFilter)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelVideos;
        private System.Windows.Forms.Label label1;
        private Emgu.CV.UI.ImageBox imageBoxPerson;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelClones;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelPeople;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxFilter;
        private System.Windows.Forms.Label label3;
        private Emgu.CV.UI.ImageBox imageBoxFilter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonApply;
    }
}