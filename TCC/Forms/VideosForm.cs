﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Emgu.CV.UI;
using TCC.Core;
using TCC.Data;
using TCC.Models;
using TCC.Util;

namespace TCC.Forms
{
    public partial class VideosForm : Form
    {
        // Vídeos a serem processados/indexados
        private List<string> Videospath = null;
        private List<ImageBox> VideosThumb = new List<ImageBox>();
        private Thread RecognizerThread = null;
        private System.Windows.Forms.Timer LoadTimer = new System.Windows.Forms.Timer();

        public VideosForm()
        {
            InitializeComponent();

            LoadTimer.Tick += LoadForm;
            LoadTimer.Start();

            OnSizeChanged(null);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            RecognizerThread.Abort();
            DataFile.SaveFiles();
        }

        private void LoadForm(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            Videospath = new List<string>(Directory.EnumerateFiles(DataFile.config.VideosDirectory, DataFile.config.VideosExtensions, SearchOption.AllDirectories));
            for (int idx = 0; idx < Videospath.Count; ++idx)
            {
                string videoPath = Videospath[idx];

                string hash = MD5Facade.CalculateMD5(videoPath);
                if (!DataFile.videos.TryGetValue(hash, out Video video))
                {
                    video = new Video(videoPath);
                    DataFile.videos.Add(hash, video);
                }

                ImageBox imageBox = new ImageBox
                {
                    Image = video.Thumb,
                    Size = new Size(DataFile.config.VideoThumbSize, DataFile.config.VideoThumbSize),
                    FunctionalMode = ImageBox.FunctionalModeOption.Minimum,
                    Name = videoPath,
                };

                if (video.Indexed)
                    flowLayoutPanelIndexados.Controls.Add(imageBox);
                else
                    flowLayoutPanelPendentes.Controls.Add(imageBox);

                imageBox.MouseDoubleClick += Video_MouseDoubleClick;
                VideosThumb.Add(imageBox);
            }

            RecognizerThread = new Thread(RecognizerThreadMethod);
            RecognizerThread.Start();

            Cursor.Current = Cursors.Default;
            LoadTimer.Stop();
        }

        private void Video_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ImageBox image = (ImageBox)sender;

            RecognizerThread.Abort();

            string hash = MD5Facade.CalculateMD5(image.Name);
            if (!DataFile.videos.TryGetValue(hash, out Video video))
            {
                video = new Video(image.Name);
                DataFile.videos.Add(hash, video);
            }
            video.Showing = true;

            RecognizerThread = new Thread(RecognizerThreadMethod);
            RecognizerThread.Start();

            Thread thread = new Thread(new ParameterizedThreadStart(RunVideoForm));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start(image.Name);
        }

        private void RunVideoForm(object videoPath)
        {
            Application.Run(new VideoForm((string)videoPath, 0));
        }

        private void RecognizerThreadMethod()
        {
            for (int idx = 0; idx < Videospath.Count; ++idx)
            {
                string hash = MD5Facade.CalculateMD5(Videospath[idx]);
                if (DataFile.videos.TryGetValue(hash, out Video video) && (video.Indexed || video.Showing))
                    continue;

                Recognizer recognizer = new Recognizer(Videospath[idx], null, 0);

                Invoke((MethodInvoker)(() => { flowLayoutPanelPendentes.Controls.Remove(VideosThumb[idx]); }));
                Invoke((MethodInvoker)(() => { flowLayoutPanelIndexados.Controls.Add(VideosThumb[idx]); }));

                DataFile.SaveFiles();
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            int x = splitContainer1.Location.X;

            labelPendentes.Location = new Point(x, labelPendentes.Location.Y);

            x += splitContainer1.SplitterDistance;
            x += splitContainer1.SplitterWidth;

            labelIndexados.Location = new Point(x, labelIndexados.Location.Y);
        }

        private void SplitContainer1_SplitterMoved(object sender, System.Windows.Forms.SplitterEventArgs e)
        {
            OnSizeChanged(null);
        }
    }
}
