﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using TCC.Data;

namespace TCC.Forms
{
    public partial class ConfigForm : Form
    {
        public ConfigForm()
        {
            InitializeComponent();

            textBoxFonte.DataBindings.Add("Text", DataFile.config, "FontName", false, DataSourceUpdateMode.OnPropertyChanged);
            textBoxFonteSize.DataBindings.Add("Text", DataFile.config, "FontSize", false, DataSourceUpdateMode.OnPropertyChanged);
            buttonCorFonte.BackColor = DataFile.config.ColorFont;
            buttonCorRetangulo.BackColor = DataFile.config.RectangleColor;
            textBoxEspessura.DataBindings.Add("Text", DataFile.config, "RectangleThickness", false, DataSourceUpdateMode.OnPropertyChanged);

            textBoxVideos.DataBindings.Add("Text", DataFile.config, "VideosDirectory", false, DataSourceUpdateMode.OnPropertyChanged);
            textBoxVideosExt.DataBindings.Add("Text", DataFile.config, "VideosExtensions", false, DataSourceUpdateMode.OnPropertyChanged);
            textBoxFaces.DataBindings.Add("Text", DataFile.config, "FacesDirectory", false, DataSourceUpdateMode.OnPropertyChanged);
            textBoxFacesExt.DataBindings.Add("Text", DataFile.config, "FacesExtension", false, DataSourceUpdateMode.OnPropertyChanged);

            FillHaarCombo();
            comboBoxHaarCascade.DataBindings.Add("SelectedItem", DataFile.config, "CascadeClassifier", false, DataSourceUpdateMode.OnPropertyChanged);

            textBoxTamanhoRetanguloFace.DataBindings.Add("Text", DataFile.config, "FaceRectangleSize", false, DataSourceUpdateMode.OnPropertyChanged);
            textBoxEscalaRedimensionamento.DataBindings.Add("Text", DataFile.config, "ResizeScale", false, DataSourceUpdateMode.OnPropertyChanged);

            textBoxThreshold.DataBindings.Add("Text", DataFile.config, "LBPH_threshold", false, DataSourceUpdateMode.OnPropertyChanged);
            textBoxRadius.DataBindings.Add("Text", DataFile.config, "LBPH_radius", false, DataSourceUpdateMode.OnPropertyChanged);
            textBoxNeighbors.DataBindings.Add("Text", DataFile.config, "LBPH_neighbors", false, DataSourceUpdateMode.OnPropertyChanged);
            textBoxGridX.DataBindings.Add("Text", DataFile.config, "LBPH_gridX", false, DataSourceUpdateMode.OnPropertyChanged);
            textBoxGridY.DataBindings.Add("Text", DataFile.config, "LBPH_gridY", false, DataSourceUpdateMode.OnPropertyChanged);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            DataFile.SaveFiles();
        }

        private void FillHaarCombo()
        {
            IEnumerable<string> files = Directory.EnumerateFiles(DataFile.config.HaarCascadeDirectory, "*.xml");
            foreach (string file in files)
                comboBoxHaarCascade.Items.Add(file.Replace(DataFile.config.HaarCascadeDirectory, ""));
        }

        private void ButtonCorFonte_Click(object sender, System.EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.Color = DataFile.config.ColorFont;

            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
                DataFile.config.ColorFont = MyDialog.Color;
                buttonCorFonte.BackColor = DataFile.config.ColorFont;
            }
        }

        private void ButtonCorRetangulo_Click(object sender, System.EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.Color = DataFile.config.RectangleColor;

            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
                DataFile.config.RectangleColor = MyDialog.Color;
                buttonCorRetangulo.BackColor = DataFile.config.RectangleColor;
            }
        }
    }
}
