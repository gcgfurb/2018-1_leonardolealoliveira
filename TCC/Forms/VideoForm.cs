﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using TCC.Core;
using TCC.Data;
using TCC.Models;
using TCC.Util;

namespace TCC.Forms
{
    public partial class VideoForm : Form
    {
        private Recognizer recognizer = null;
        public VideoForm(string videoPath, int idPerson)
        {
            InitializeComponent();

            imageBox1.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            recognizer = new Recognizer(videoPath, imageBox1, idPerson);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            DataFile.SaveFiles();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            string hash = MD5Facade.CalculateMD5(recognizer.VideoPath);
            DataFile.videos.TryGetValue(hash, out Video video);
            video.Showing = false;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            switch (e.KeyCode)
            {
                case Keys.Right: recognizer.Right(); break;
                case Keys.Space: recognizer.PlayPause(); break;
            }
        }
    }
}
