﻿using System;
using System.Threading;
using System.Windows.Forms;
using TCC.Data;

namespace TCC.Forms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            DataFile.SaveFiles();
        }

        private void PeobleButton_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(RunPeopleForm);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void RunPeopleForm()
        {
            Application.Run(new PeopleForm());
        }

        private void VideosButton_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(RunVideosForm);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }
        private void RunVideosForm()
        {
            Application.Run(new VideosForm());
        }

        private void ConfigButton_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(RunConfigForm);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void RunConfigForm()
        {
            Application.Run(new ConfigForm());
        }
    }
}
