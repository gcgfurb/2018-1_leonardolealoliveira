﻿namespace TCC.Forms
{
    partial class VideosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanelPendentes = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanelIndexados = new System.Windows.Forms.FlowLayoutPanel();
            this.labelPendentes = new System.Windows.Forms.Label();
            this.labelIndexados = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(12, 32);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanelPendentes);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.flowLayoutPanelIndexados);
            this.splitContainer1.Size = new System.Drawing.Size(776, 406);
            this.splitContainer1.SplitterDistance = 381;
            this.splitContainer1.SplitterWidth = 10;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.TabStop = false;
            this.splitContainer1.SplitterMoved += SplitContainer1_SplitterMoved;
            // 
            // flowLayoutPanelPendentes
            // 
            this.flowLayoutPanelPendentes.AutoScroll = true;
            this.flowLayoutPanelPendentes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelPendentes.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelPendentes.Name = "flowLayoutPanelPendentes";
            this.flowLayoutPanelPendentes.Size = new System.Drawing.Size(379, 404);
            this.flowLayoutPanelPendentes.TabIndex = 0;
            // 
            // flowLayoutPanelIndexados
            // 
            this.flowLayoutPanelIndexados.AutoScroll = true;
            this.flowLayoutPanelIndexados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelIndexados.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelIndexados.Name = "flowLayoutPanelIndexados";
            this.flowLayoutPanelIndexados.Size = new System.Drawing.Size(383, 404);
            this.flowLayoutPanelIndexados.TabIndex = 0;
            // 
            // labelPendentes
            // 
            this.labelPendentes.AutoSize = true;
            this.labelPendentes.Location = new System.Drawing.Point(12, 16);
            this.labelPendentes.Name = "labelPendentes";
            this.labelPendentes.Size = new System.Drawing.Size(61, 13);
            this.labelPendentes.TabIndex = 1;
            this.labelPendentes.Text = "Pendentes:";
            // 
            // labelIndexados
            // 
            this.labelIndexados.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelIndexados.Location = new System.Drawing.Point(401, 16);
            this.labelIndexados.Name = "labelIndexados";
            this.labelIndexados.Size = new System.Drawing.Size(59, 13);
            this.labelIndexados.TabIndex = 2;
            this.labelIndexados.Text = "Indexados:";
            // 
            // VideosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelIndexados);
            this.Controls.Add(this.labelPendentes);
            this.Controls.Add(this.splitContainer1);
            this.Name = "VideosForm";
            this.Text = "Vídeos";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelPendentes;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelIndexados;
        private System.Windows.Forms.Label labelPendentes;
        private System.Windows.Forms.Label labelIndexados;
    }
}