﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Emgu.CV;
using Emgu.CV.Structure;
using Newtonsoft.Json;
using TCC.Models;

namespace TCC.Data
{
    public static class DataFile
    {
        private static string peopleFileName = "people.json";
        private static string videosFileName = "videos.json";
        private static string configFileName = "config.json";

        public static Dictionary<string, Video> videos = new Dictionary<string, Video>();
        public static List<Person> people = new List<Person>();
        public static Config config = new Config();

        private static Mutex mutex = new Mutex();

        public static Person GetPerson(int id)
        {
            for (int idx = 0; idx < people.Count; ++idx)
            {
                Person person = people[idx];

                if (person.Id == id)
                    return person;

                if (GetPerson(id, person))
                    return person;
            }

            return null;
        }

        private static bool GetPerson(int id, Person person)
        {
            for (int idx = 0; idx < person.Clones.Count; ++idx)
            {
                Person clone = person.Clones[idx];

                if (clone.Id == id)
                    return true;

                if (GetPerson(id, clone))
                    return true;
            }

            return false;
        }

        public static void LoadFiles()
        {
            try
            {
                if (File.Exists(videosFileName))
                    videos = JsonConvert.DeserializeObject<Dictionary<string, Video>>(File.ReadAllText(videosFileName));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                if (File.Exists(peopleFileName))
                    people = JsonConvert.DeserializeObject<List<Person>>(File.ReadAllText(peopleFileName));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                if (File.Exists(configFileName))
                    config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(configFileName));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void SaveFiles()
        {
            bool videosWrited = false;
            bool peopleWrited = false;
            bool configWrited = false;

            for (int idx = 0; idx < 100; ++idx)
            {
                mutex.WaitOne();

                if (!videosWrited)
                {
                    try
                    {
                        File.WriteAllText(videosFileName, JsonConvert.SerializeObject(videos, Formatting.Indented));
                        videosWrited = true;
                    }
                    catch (Exception) { }
                }

                if (!peopleWrited)
                {
                    try
                    {
                        File.WriteAllText(peopleFileName, JsonConvert.SerializeObject(people, Formatting.Indented));
                        peopleWrited = true;
                    }
                    catch (Exception) { }
                }

                if (!configWrited)
                {
                    try
                    {
                        File.WriteAllText(configFileName, JsonConvert.SerializeObject(config, Formatting.Indented));
                        configWrited = true;
                    }
                    catch (Exception) { }
                }

                mutex.ReleaseMutex();

                if (videosWrited && peopleWrited && configWrited)
                    break;
            }

            if (!videosWrited || !peopleWrited || !configWrited)
                throw new Exception("Falha ao gravar arquivos");
        }

        public static void LoadTrainingData(List<int> TrainLabels, List<Image<Gray, Byte>> TrainImages)
        {
            for (int idx = 0; idx < people.Count; ++idx)
            {
                Person person = people[idx];

                TrainImages.Add(person.MainPhoto.Convert<Gray, Byte>());
                TrainLabels.Add(person.Id);

                for (int idxClones = 0; idxClones < person.Clones.Count; ++idxClones)
                {
                    Person clone = person.Clones[idxClones];

                    TrainImages.Add(clone.MainPhoto.Convert<Gray, Byte>());
                    TrainLabels.Add(person.Id);
                }
            }
        }
    }
}
