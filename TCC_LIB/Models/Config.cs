﻿using System.Drawing;

namespace TCC.Models
{
    public class Config
    {
        // UI
        public string FontName { get; set; }
        public int FontSize { get; set; }
        public Color ColorFont { get; set; }
        public Color RectangleColor { get; set; }
        public int RectangleThickness { get; set; }

        // system-file
        public string VideosExtensions { get; set; }
        public string VideosDirectory { get; set; }
        public string FacesDirectory { get; set; }
        public string FacesExtension { get; set; }
        public string HaarCascadeDirectory { get; set; }

        // detecção
        public string CascadeClassifier { get; set; }
        public int FaceRectangleSize { get; set; }
        public double ResizeScale { get; set; }

        // reconhecimento
        public double LBPH_threshold { get; set; }
        public int LBPH_radius { get; set; }
        public int LBPH_neighbors { get; set; }
        public int LBPH_gridX { get; set; }
        public int LBPH_gridY { get; set; }

        // Fixed
        public int PeopleFaceSize { get; set; } = 200;
        public int VideoThumbSize { get; set; } = 200;

        public Config()
        {
            // UI
            FontName = "Comic Sans MS";
            FontSize = 12;
            ColorFont = Color.Aqua;
            RectangleColor = Color.BlueViolet;
            RectangleThickness = 2;

            // system-file
            // only AVI is public supported in all devices
            // others extensions depends on codecs
            // this program was tested only on mp4 files
            VideosExtensions = "*.mp4";
            VideosDirectory = "Vídeos/";
            FacesDirectory = "Temp/";
            FacesExtension = ".png";
            HaarCascadeDirectory = "haarcascade/";

            // detecção
            CascadeClassifier = "haarcascade_frontalface_alt_tree.xml";
            FaceRectangleSize = 200;
            ResizeScale = 1.0;

            // reconhecimento
            LBPH_threshold = 80;
            LBPH_radius = 1;
            LBPH_neighbors = 8;
            LBPH_gridX = 8;
            LBPH_gridY = 8;
        }
    }
}
