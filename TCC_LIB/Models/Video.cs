﻿using System;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using TCC.Data;

namespace TCC.Models
{
    public class Video
    {
        public string FilePath { get; set; }
        public Image<Bgr, Byte> Thumb { get; set; }
        public List<Frame> Frames { get; set; }
        public bool Indexed { get; set; }
        public bool Showing { get; set; }

        public Video()
        {
        }

        public Video(string filePath)
        {
            FilePath = filePath;

            Capture capture = new Capture(FilePath);
            Thumb = new Image<Bgr, Byte>(capture.QueryFrame().Bitmap).Resize(DataFile.config.VideoThumbSize, DataFile.config.VideoThumbSize, Emgu.CV.CvEnum.Inter.Cubic);

            Frames = new List<Frame>();
        }
    }
}
