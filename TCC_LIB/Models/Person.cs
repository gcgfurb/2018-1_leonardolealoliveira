﻿using System;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;

namespace TCC.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Image<Bgr, Byte> MainPhoto { get; set; }
        public List<Person> Clones { get; set; }

        public Person()
        {
        }

        public Person(int id, Image<Bgr, Byte> mainPhoto)
        {
            Id = id;
            Name = "Pessoa: " + Id.ToString();
            MainPhoto = mainPhoto;
            Clones = new List<Person>();
        }
    }
}
