﻿using System.Drawing;

namespace TCC.Models
{
    public class Face
    {
        public int IdPerson { get; set; }
        public double Distance { get; set; }
        public Rectangle Position { get; set; }

        public Face()
        {

        }

        public Face(int idPerson, double distance, Rectangle position)
        {
            IdPerson = idPerson;
            Distance = distance;
            Position = position;
        }
    }
}
