﻿using System.Collections.Generic;

namespace TCC.Models
{
    public class Frame
    {
        public int Index { get; set; }
        public List<Face> Faces { get; set; }

        public Frame()
        {

        }

        public Frame(int index)
        {
            Index = index;
            Faces = new List<Face>();
        }
    }
}
