﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Face;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using TCC.Data;
using TCC.Models;
using TCC.Util;

namespace TCC.Core
{
    public class Recognizer
    {
        // Interface
        private ImageBox ImageBox = null;
        private bool Skipping = false;
        private int IdPerson = 0;

        // Vídeo a ser processado/indexado
        public string VideoPath { get; set; } = null;
        private Video Video = null;
        private int IndexFrame = 0;

        // detecção
        private Timer Timer = new Timer();
        private Capture Capture = null;
        private Image<Bgr, Byte> CurrentFrame = null;
        private CascadeClassifier CascadeClassifier = new CascadeClassifier(DataFile.config.HaarCascadeDirectory + DataFile.config.CascadeClassifier);

        // reconhecimento
        private FaceRecognizer FaceRecognizer = new LBPHFaceRecognizer(DataFile.config.LBPH_radius, DataFile.config.LBPH_neighbors, DataFile.config.LBPH_gridX, DataFile.config.LBPH_gridY, DataFile.config.LBPH_threshold);
        private List<int> TrainLabels = new List<int>();
        private List<Image<Gray, Byte>> TrainImages = new List<Image<Gray, byte>>();

        public Recognizer(string videoPath, ImageBox imageBox, int idPerson = 0)
        {
            VideoPath = videoPath;
            ImageBox = imageBox;
            IdPerson = idPerson;

            DataFile.LoadTrainingData(TrainLabels, TrainImages);
            if (TrainImages.Count > 0)
                FaceRecognizer.Train(TrainImages.ToArray(), TrainLabels.ToArray());

            string hash = MD5Facade.CalculateMD5(VideoPath);
            if (!DataFile.videos.TryGetValue(hash, out Video))
            {
                Video = new Video(VideoPath);
                DataFile.videos.Add(hash, Video);
                CleanDirectory();
            }

            Capture = new Capture(Video.FilePath);
            CurrentFrame = new Image<Bgr, Byte>(Capture.QueryFrame().Bitmap);

            if (IdPerson != 0)
                Right();
            else if (ImageBox == null)
            {
                while (!Video.Indexed)
                    TimerFunction(null, null);
            }
            else
            {
                Timer.Interval = 1000 / (int)Capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);
                Timer.Tick += TimerFunction;
                Timer.Start();
            }
        }

        private void TimerFunction(object sender, EventArgs e)
        {
            if (!Capture.Grab())
            {
                if (Timer.Enabled)
                    Timer.Stop();

                Video.Indexed = true;
                Skipping = false;

                if (ImageBox != null)
                    ImageBox.Image = CurrentFrame;

                return;
            }

            Capture.Retrieve(CurrentFrame);

            Frame frame = null;
            if (!FrameIndexed(ref frame) && !Video.Indexed)
                DetectAndRecognize(ref frame);

            DrawInfo(frame);

            if (frame != null)
            {
                foreach (Face face in frame.Faces)
                {
                    if (IdPerson == 0 || IdPerson == face.IdPerson)
                    {
                        Skipping = false;
                        break;
                    }
                }
            }

            ++IndexFrame;
            if (ImageBox != null && !Skipping)
                ImageBox.Image = CurrentFrame;
        }

        private bool FrameIndexed(ref Frame outFrame)
        {
            foreach (Frame frame in Video.Frames)
            {
                if (frame.Index == IndexFrame)
                {
                    outFrame = frame;
                    return true;
                }

                if (frame.Index > IndexFrame)
                    return true;
            }

            return false;
        }

        private void DrawInfo(Frame frame)
        {
            if (frame == null)
                return;

            foreach (Face face in frame.Faces)
            {
                CurrentFrame.Draw(face.Position, new Bgr(DataFile.config.RectangleColor), DataFile.config.RectangleThickness);

                Graphics g = Graphics.FromImage(CurrentFrame.Bitmap);
                g.DrawString(DataFile.GetPerson(face.IdPerson).Name, new Font(DataFile.config.FontName, DataFile.config.FontSize), new SolidBrush(DataFile.config.ColorFont), new Point(face.Position.Location.X, face.Position.Location.Y));
            }
        }

        private void DetectAndRecognize(ref Frame outFrame)
        {
            outFrame = new Frame(IndexFrame);

            Image<Gray, Byte> currentGrayFrame = CurrentFrame.Convert<Gray, Byte>();
            currentGrayFrame = currentGrayFrame.Resize(DataFile.config.ResizeScale, Emgu.CV.CvEnum.Inter.Cubic);

            Rectangle[] facesDetected = CascadeClassifier.DetectMultiScale(currentGrayFrame);
            foreach (Rectangle face in facesDetected)
            {
                Rectangle originalFace = face;
                originalFace.X = (int)(originalFace.X * (1 / DataFile.config.ResizeScale));
                originalFace.Y = (int)(originalFace.Y * (1 / DataFile.config.ResizeScale));
                originalFace.Width = (int)(originalFace.Width * (1 / DataFile.config.ResizeScale));
                originalFace.Height = (int)(originalFace.Height * (1 / DataFile.config.ResizeScale));

                Image<Gray, Byte> faceRegion = currentGrayFrame.Copy(face);
                faceRegion = faceRegion.Resize(DataFile.config.FaceRectangleSize, DataFile.config.FaceRectangleSize, Emgu.CV.CvEnum.Inter.Cubic);

                int idPerson = 0;
                if (TrainImages.Count == 0)
                {
                    idPerson = TrainLabels.Count + 1;

                    TrainImages.Add(faceRegion);
                    TrainLabels.Add(idPerson);
                    FaceRecognizer.Train(TrainImages.ToArray(), TrainLabels.ToArray());

                    SaveFace(CurrentFrame.Copy(originalFace), originalFace, idPerson);
                }

                FaceRecognizer.PredictionResult predictionResult = FaceRecognizer.Predict(faceRegion);

                if (predictionResult.Label == 0)
                {
                    idPerson = TrainLabels.Count + 1;

                    TrainImages.Add(faceRegion);
                    TrainLabels.Add(idPerson);
                    FaceRecognizer.Train(TrainImages.ToArray(), TrainLabels.ToArray());

                    SaveFace(CurrentFrame.Copy(originalFace), originalFace, idPerson);
                }
                else
                    idPerson = predictionResult.Label;

                outFrame.Faces.Add(new Face(idPerson, predictionResult.Distance, originalFace));
            }

            if (outFrame.Faces.Count > 0)
                Video.Frames.Add(outFrame);
        }

        private void SaveFace(Image<Bgr, byte> face, Rectangle position, int label)
        {
            string filePath = DataFile.config.FacesDirectory;
            filePath = Path.Combine(filePath, Path.GetFileNameWithoutExtension(VideoPath));
            filePath = Path.Combine(filePath, label + DataFile.config.FacesExtension);

            if (File.Exists(filePath))
                File.Delete(filePath);

            string root = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            face.Save(filePath);

            DataFile.people.Add(new Person(label, face));
        }

        private void CleanDirectory()
        {
            string directoryPath = Path.Combine(DataFile.config.FacesDirectory, Path.GetFileNameWithoutExtension(VideoPath));
            if (Directory.Exists(directoryPath))
                Directory.Delete(directoryPath, true);
        }

        public void PlayPause()
        {
            if (Timer.Enabled)
                Timer.Stop();
            else
                Timer.Start();
        }

        public void Right()
        {
            Cursor.Current = Cursors.WaitCursor;

            if (Timer.Enabled)
                Timer.Stop();

            Skipping = true;
            while (Skipping)
                TimerFunction(null, null);

            Cursor.Current = Cursors.Default;
        }
    }
}
